# I - Ipmportation des modules
import string

# II - Variables
n = 22733
d = 13193
k = 1
y = 5159

# III - Fonctions
# 1 - Convertion en binaire de la puissance d
def Binary(d):
    key = bin(d).replace("0b", "")
    return key
# 2 - Calcule de y a la puissance d (square en multiply)
def SquareAndMultiply(y, key, n):
    p = 1
    for k in key:
        if k == "1":
            p = ((p**2)*y)%n
        else:
            p = (p**2)%n
    return p
# 3 - Decomposition en puissance 26 du resultat de square and multiply
def Decomposition(p):
    exp = []
    while p > 0:
        if p < 26:
            exp.append(p)
            p = 0
        else:
            p, r = p//26, p%26
            exp.append(r)
    return tuple(exp)
# 4 - Creation du dictionnaire de correspondance 
def Dictionnary(k):
    asc = string.ascii_uppercase
    dic = {}
    for letter, i in zip(asc, range(len(asc))):
        dic[i+k] = letter
    return dic
# 5 - algorithme du RSA
def RivestShamirAdleman(k, y, d, n):
    msg, x = [], ""
    key = Binary(d)
    p = SquareAndMultiply(y, key, n)
    exp = Decomposition(p)
    dic = Dictionnary(k)
    for item in exp:
        msg.append(dic[item])
    for item in msg:
        x += item
    return x

# IV - Programme principal
if __name__ == "__main__":
    x = RivestShamirAdleman(k, y, d, n)
    print(f"Le message est : {x}")